### Autor Diego Navia <diego.navia@icloud.com>
# Guía para transformar a integración un proyecto Java
## Herramientas necesarias
- JDK 1.8 y JDK 1.6
- Maven 3.X o posterior
- Eclipse 

## Transformar a proyecto Maven
1. Agregar archivo pom.xml a la raiz del proyecto.

Ejemplo:
```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>cl.liberty.negocio</groupId>
	<artifactId>nombreproyecto</artifactId>
	<version>1.0.0</version>
	<packaging>war</packaging>
	<!-- 
	 <dependencies>
		
		<dependency>
		    <groupId>?</groupId>
		    <artifactId>?</artifactId>
		    <version>?</version>
		</dependency>
		
	 </dependencies>
	  -->
	<build>
		<sourceDirectory>src</sourceDirectory>
		<resources>
			<resource>
				<directory>src</directory>
				<excludes>
					<exclude>**/*.java</exclude>
				</excludes>
			</resource>
		</resources>
		<plugins>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.3</version>
				<configuration>
					<warSourceDirectory>WebContent</warSourceDirectory>
				</configuration>
			</plugin>
		</plugins>
	</build>
</project>
```

2. Configurar el Build Path del proyecto 

* Propiedades del proyecto -> Libraries -> Debe estar solamente JRE System Library JDK 1.8 y Maven Dependencies
* Despues en la columna izquierda de las propiedades -> Java Compiler -> Seleccionar JDK 1.8 y guardar.


3. Ejecutar Maven Clean y Maven Install en Run As, para revisar que maven esta funcionando.
4. Agregar librerias necesarias para el funcionamiento del proyecto.

* Agregar librerias de maven central
* Agregas librerias propias con el siguiente comando:

```
   mvn install:install-file 
      -Dfile=\ruta\libreria_ejemplo.jar 
      -DgroupId=cl.liberty.negocio 
      -DartifactId=libreria-core-ejemplo 
      -Dversion=1.0.0 
      -Dpackaging=jar 
      -DgeneratePom=true
      
```
  
  
  
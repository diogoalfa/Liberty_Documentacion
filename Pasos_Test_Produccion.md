### Autor: Diego Navia <diego.navia@icloud.com>
# Guía para promover proyectos con integración continua a Test y Producción
## Pasos para promover ambiente TEST
gnsis-tickets(jira):https://gnsis-it.atlassian.net/servicedesk
1. Solicitud de creación de branch :  jira -> svn ->  Creación de rama -> llenar formulario.
  - Crea la rama a partir del trunk.
2. Crear linea de desarrollo IC : jira -> Linea de Desarrollo -> Crear Linea Desarrollo IC -> llenar formulario.
  - Creación de un espacio de trabajo de jenkins integración continua.
  - Para continuar al siguiente paso, el proyecto NO debe tener issues Blocker y Critical en SonarQube.
3. Terminado el desarrollo se necesita obtener el artifacto en nexus : jira -> ciclo de vida -> Análisis de paso a Testing.
  - Se crea el documento Anexo_Traspaso_Web_Form_Actualizado (contiene la ruta del artifacto en nexus test).
4. Instalar artifacto en ambiente desarrollo para pruebas propias, y posteriormente crear el documento de evidencia de ambiente desarrollo.
5. Con la evidencia de pruebas en ambiente desarrollo, solicita autorización a QA para instalación en ambiente test.
  - Se obtiene correo de aprobación de QA para instalación a Test.
6. Dexon de paso a test asignado a Change Manager 
 
* Horarios de instalación 8:00 - 9:00 AM y 16:00 - 17:00 PM 
* Debe contener los siguientes documentos:
  - Aprobación de QA para paso a Test (correo).
  - Evidencias de pruebas en desarrollo (doc).
  - Anexo_traspaso_web_form_actualizado (excel).
  
## Pasos para promover ambiente Producción 
1. Cuando el proyecto este correctamente instalado en ambiente test, QA procedera a testear el proyecto, y entregar las pruebas realizadas.
  - Aprobación de QA para paso producción (pdf)
2. Solicitar al usuario que validar-probar cambios realizados y llenar documento de pruebas.
  - Aprobación de usuario con documento Anexo Prueba (xlsx), con capturas y estado de pruebas.
3. Subir branch al trunk para paso a producción desde el trunk : jira -> ciclo de vida -> Subir cambios desde Branch al Trunk -> llenar formulario.
  - Entrega del trunk con la revisión generada.
4. Obtener el artifacto de nexus release para paso a producción : jira -> ciclo de vida -> Paso a producción Java -> llenar formulario.  
  - Se actualiza el documento Anexo_traspaso_web_form_actualizado ( contiene las ruta del artifacto en nexus release).
5. Dexon para paso a producción debe ser asignado al Change Manager. 

* Los Martes y Jueves son los días habilitados para paso a producción y deben enviar el dexon con la documentación antes de las 16:00 PM.
* Debe contener los siguientes documentos:
  - Aprobación de QA (pdf).
  - Aprobación de Usuario Anexo_pruebas (excel).
  - Anexo_traspaso_web_form_actualizado 
  - RFC
  - CAB